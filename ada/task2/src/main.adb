with Sq_Pack, Sq_Pack.rec_pack;
use Sq_Pack, Sq_Pack.rec_pack;

procedure Main is
   Sq1 : Square;
   Rec1 : Rectangle;
begin
   init(Sq1, 2);
   Print(Sq1);

  init(Rec1, 3, 4);
  Print(Rec1);

end Main;
