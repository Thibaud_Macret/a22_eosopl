with Text_IO, Ada.Integer_Text_IO;
use Text_IO, Ada.Integer_Text_IO;

package body sq_pack is
   procedure Init(Sq : out Square'class; SqLength : in Integer) is 
   begin
      Sq.Length := SqLength;
   end Init;
   
   function getLen(Sq : in Square) return Integer is
   begin
      return Sq.Length;
   end getLen;
   
   
   procedure Print(Sq : in Square) is
   begin
      Put("Square.");
      New_Line;
      Put("Len :");
      Put(getLen(Sq), 1);
      New_Line;
      Put("Perimeter :");
      Put(getPerimeter(Sq), 1);
      New_Line;
      Put("Area :");
      Put(getArea(Sq), 1);
      New_Line;
   end Print;
   
   function GetPerimeter(Sq : in Square) return Integer is
   begin
      return Sq.Length*4;
   end GetPerimeter;
   
   function GetArea(Sq : in Square) return Integer is
   begin
      return Sq.Length*Sq.Length;
   end GetArea;
   
     
end sq_pack;
