package Sq_Pack is

   type Square is tagged private;
   procedure Init(Sq : out Square'class; SqLength : in Integer);
   procedure Print(Sq : in Square);
   function getLen(Sq : in Square) return Integer;
   function GetPerimeter(Sq : in Square) return Integer;
   function GetArea(Sq : in Square) return Integer;

private
   type Square is tagged record
      Length : Integer;
   end record;

end Sq_Pack;
