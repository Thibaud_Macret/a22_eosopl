with Text_IO, Ada.Integer_Text_IO;
use Text_IO, Ada.Integer_Text_IO;

package body sq_pack.rec_pack is

   procedure Init(Rec : out Rectangle; Len : in Integer; height : in Integer) is
   begin
      Sq_Pack.Init(Rec, Len);
      Rec.height := height;
   end Init;
   
   procedure Print(Rec : in Rectangle) is
   begin
      Put("Rectangle.");
      New_Line;
      Put("Len :");
      Put(Rec.Length, 1);
      New_Line;
      Put("Height :");
      Put(Rec.Height, 1);
      New_Line;
      Put("Perimeter :");
      Put(getPerimeter(Rec), 1);
      New_Line;
      Put("Area :");
      Put(getArea(Rec), 1);
      New_Line;
   end Print;
   
   function GetPerimeter(Rec : in Rectangle) return Integer is
   begin
      return ((Rec.height + Rec.Length) * 2);
   end GetPerimeter;
   
   function GetArea(Rec : in Rectangle) return Integer is
   begin
      return (Rec.height * Rec.Length);
   end GetArea;

end sq_pack.rec_pack;
