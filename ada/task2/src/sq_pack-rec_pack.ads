package sq_pack.rec_pack is
   type Rectangle is new Sq_Pack.Square with private;
   
   procedure Init(Rec : out Rectangle; Len : in Integer; height : in Integer);
   procedure Print(Rec : in Rectangle);
   function GetPerimeter(Rec : in Rectangle) return Integer;
   function GetArea(Rec : in Rectangle) return Integer;
   
private
   type Rectangle is new Sq_Pack.Square with record
      Height : Integer;
   end record;

end sq_pack.rec_pack;
