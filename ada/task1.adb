with Text_IO;
use Text_IO;

procedure main is
   type hundred is new Integer range 0..100;
   type MatrixType is array (Character range <>, Character range <>) of hundred;
   Matr : MatrixType('a'..'d', 'e'..'i') := ( 'a'..'b' => ('e' => 10, 'g' => 54, others => 0), others => ('f'..'g' => 76, others => 3));

   package hundred_IO is new Integer_IO(hundred);
   use hundred_IO;

   procedure Print(matrix : MatrixType) is
      row : Character := matrix'First;
      col : Character := matrix'First(2);
   begin
      while(row /= Character'Succ(matrix'Last)) loop
         while(col /= Character'Succ(matrix'Last(2))) loop
            Put(matrix(row, col));
            col := Character'Succ(col);
         end loop;
         New_Line;
         row := Character'Succ(row);
         col := matrix'First(2);
      end loop;
   end Print;

   function search_max(matrix : MatrixType) return hundred is
      max : hundred := matrix(matrix'First(1), matrix'First(2));
   begin
      for row in matrix'First(1)..matrix'Last(1)  loop
         for col in matrix'First(2)..matrix'Last(2)  loop
            if(max < matrix(row, col)) then
               max := matrix(row, col);
            end if;
         end loop;
      end loop;
      return max;
   end search_max;

   procedure check_data(matrix : MatrixType) is
      UncorectValue : Exception;
   begin
      for row in matrix'First(1)..matrix'Last(1)  loop
         for col in matrix'First(2)..matrix'Last(2)  loop
            if(matrix(row, col) = 0) then
               raise UncorectValue;
            end if;
         end loop;
      end loop;

   exception
      when UncorectValue =>
         Put("Matix contains at least one zero");
   end;

begin

   Print(Matr);
   New_Line;
   Put("Maximum : ");
   Put(search_max(Matr));
   New_Line;
   check_data(Matr);

end main;
