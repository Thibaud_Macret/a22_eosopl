with Text_IO, Ada.Integer_Text_IO;
use Text_IO, Ada.Integer_Text_IO;

procedure Main is

   type MType is array(Integer range<>, Integer range<>) of Integer;
   matrix : MType(0..6, 0..5) := ((1,1,1,1,1,10),(2,2,20,2,2,2),(3,3,30,3,3,3),(40,4,4,4,4,4),(5,5,5,5,5,50),(6,6,6,60,6,6),(7,7,7,70,7,7));

   task MaxElementInRow is
      entry TotalRows(rows : in Integer);
      entry Row(index : in Integer);
   end MaxElementInRow;

   task ListOfRows is
      entry Result(Max : in Integer);
   end ListOfRows;

   task body MaxElementInRow is
      ttlrows : Integer := 0;
      actualRow : Integer := 0;
      maximum : Integer := -1;
      element : Integer := 0;
      nbDenials : Integer := 0;
   begin
      delay(0.001);
      accept TotalRows (rows : in Integer) do
         ttlrows := rows;
      end TotalRows;

      while (actualRow < ttlrows) loop
select
         accept Row (index : in Integer) do
            actualRow := index;
         end Row;

         element := 0;

         if(element = 0) then
            maximum := matrix(actualRow, matrix'First(2));
            while element < matrix'Last(1) loop
               if(matrix(actualRow, element) > maximum) then
                  maximum := matrix(actualRow, element);
               end if;
               element := element + 1;
            end loop;
               Put_Line("Task 'MaxElementInRow': waiting in 'Row' = " & Integer'Image(nbDenials));
               ListOfRows.Result(maximum);
            or
               delay(0.02);
               Put('#');
               nbDenials := nbDenials + 1;
            end select;

         end if;
      end loop;
   end MaxElementInRow;

   task body ListOfRows is
      index : Integer := 0;
      maximum : Integer := 0;
      nbDenials : Integer := 0;
   begin
      loop
         select
            MaxElementInRow.TotalRows(matrix'Last(2)+1);
            exit;
         else
              nbDenials := nbDenials + 1;
              Put('*');
         end select;
      end loop;

      Put_Line("Task 'ListOfRows': TOTAL waiting of entry 'TotalRequests' = " & Integer'Image(nbDenials));
      while index <= matrix'Last(1) loop
         MaxElementInRow.Row(index);
         index := index + 1;


         accept Result (max : in Integer) do
              maximum := max;
         end Result;

         Put_Line("Row :" & Integer'Image(index) & " Maximal element : " & Integer'Image(maximum));

         delay 0.5;
      end loop;
   end ListOfRows;

begin
   null;
end Main;
