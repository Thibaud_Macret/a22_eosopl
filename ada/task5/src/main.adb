with Ada.Integer_Text_IO, Text_IO;
use Ada.Integer_Text_IO, Text_IO;
procedure Main is
   R : constant Integer := 6;
   C : constant Integer := 7;
   type MatrixType is array
     (Integer range <>, Integer range <>)
     of Integer;
   Matr : MatrixType(1..R, 1..C) :=
     ((1,1,5,1,1,1,1),(2,2,2,2,20,2,2),(3,3,3,3,3,3,3),
      (4,4,4,4,4,4,4),(0,5,5,5,5,5,5),(6,6,1,6,6,6,6));

   task MaxElementInRow is
      entry TotalRows(N : in Integer);
      entry Row(i : in Integer);
   end MaxElementInRow;

   task ListOfRows is
       entry Result(Max : in Integer);
   end ListOfRows;

   task body MaxElementInRow is
      total : Integer := 0;
      current : Integer := 0;
      maxElement : Integer :=0;
      j : Integer := 1;
      i : Integer := 0;
      nbDenials : Integer := 0;
   begin
      delay 0.001;
      accept TotalRows (N : in Integer) do
         total := N;
      end TotalRows;
      while i < total loop
         select
            accept Row (i : in Integer) do
               current := i;
            end Row;
            j :=1;
            maxElement := Matr(current, Matr'First(2));
            while j <= Matr'Length(2) loop
               if (maxElement < Matr(current, j)) then
                  maxElement := Matr(current,j);
               end if;
               j := j +1;
            end loop;
            i := i +1;
            New_Line;
            Put_Line("Task 'MaxElementInRow' : waiting in 'Row' = " & Integer'Image(nbDenials));
            ListOfRows.Result(maxElement);
         or
            delay 0.01;
            Put("#");
            nbDenials := nbDenials + 1;
         end select;
         end loop;
   end MaxElementInRow;

   task body ListOfRows is
      maxelement: Integer := 0;
      result1: Integer := 0;
      i : Integer :=1;
      nbDenials : Integer := 0;
   begin
      loop
         select
            MaxElementInRow.TotalRows(Matr'Length);
            exit;
         else
            Put("*");
            nbDenials := nbDenials + 1;
         end select;
      end loop;
      New_Line;
      Put_Line("Task 'ListOfRows': TOTAL wiating for entry 'Totalrequests' = " & Integer'Image(nbDenials));
      while i <= Matr'Length loop
         delay 0.1;
         MaxElementInRow.Row(i);
         accept Result(Max : in Integer) do
            result1 := Max;
         end Result;
         Put_Line("Row: " & Integer'Image(i) &". Max element : " & Integer'Image(result1));
         i := i+1;
      end loop;

   end ListOfRows;

begin
   null;
end Main;
