with Text_IO, Ada.Integer_Text_IO;
use Text_IO, Ada.Integer_Text_IO;

procedure Main is

   type MType is array(Integer range<>, Integer range<>) of Integer;
   Matrix : MType(0..6, 0..5) := ((1,2,3,4,5,6),(-2,-3,-4,-5,-6,-7),(2,2,2,3,3,3),(7,6,5,5,6,7),(1,3,8,9,1,7),(8,3,8,5,6,8),(6,2,6,5,6,7));

   task type count_row(firstRow, devDelay, divDelay : Integer);

   task body count_row is
      sum : Integer := 0;
   begin
      for row in 0..matrix'Last(1)/2-1 loop
         for col in 1..matrix'Last(2) loop
            sum := sum + Matrix(row*2+firstRow, col);
         end loop;
         Put_Line("Sum of col" & Integer'Image(row*2+firstRow) & ":" & Integer'Image(sum));
         sum := 0;
         delay Duration(devDelay)/Duration(divDelay);
      end loop;
   end count_row;

   type dyn_task is access count_row;
   taskEven, taskOdd : dyn_task;

begin

   taskEven := new count_row(0,1,5);
   taskOdd := new count_row(1,1,30);

end Main;
