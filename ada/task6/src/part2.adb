with Text_IO, Ada.Integer_Text_IO;
use Text_IO, Ada.Integer_Text_IO;

procedure Main is

   type Matrix is array(Integer range<>, Integer range<>) of Integer;
   Matr1 : Matrix(0..6, 0..5) := ((1,2,3,4,5,6),(-2,-3,-4,-5,-6,-7),(2,2,2,3,3,3),(7,6,5,5,6,7),(1,3,8,9,1,7),(8,3,8,5,6,8),(6,2,6,5,6,7));

   procedure PrintRow(row : Integer) is
   begin
      Put("Row : ");
      Put(Integer'Image(row));
      Put(". Elements :");
      for element in Matr1'First(2)..Matr1'Last(2) loop
            Put(Matr1(row, element), 2);
      end loop;
      New_Line;
   end PrintRow;

   task PrintEven;
   task PrintOdd;

   task type Semaphore is
      entry STOP;
      entry GO;
   end Semaphore;

   task body Semaphore is
   begin
      for index in Matr1'First(1)..Matr1'Last(1) loop
         accept STOP;
         accept GO;
      end loop;
   end Semaphore;

   Sem : Semaphore;

   task body PrintEven is
      index : Integer := 0;
   begin
      while index <= Matr1'Last(1) loop
         Sem.STOP;
         PrintRow(index);
         Sem.GO;
         index := index + 2;
      end loop;
   end PrintEven;

   task body PrintOdd is
      index : Integer := 1;
   begin
      while index <= Matr1'Last(1) loop
         Sem.STOP;
         PrintRow(index);
         Sem.GO;
         index := index + 2;
      end loop;
   end PrintOdd;

begin
   null;
end Main;
