with Text_IO, Ada.Integer_Text_IO;
use Text_IO; 
use Ada.Integer_Text_IO;

procedure task0 is
   type MatrixType is array (Integer range <>, Integer range <>) of Integer;
   Matr : MatrixType(1..2, 1..3) := ( (10, -20, 0), (40, -50, 60));
   NbPositive : Integer := 0;
   NbNegative : Integer := 0;
   NbZeros : Integer := 0;
   
begin
   
   for i in Matr'Range(1) loop
      for j in Matr'Range(2) loop 
         if(Matr(i, j) > 0) then
           NbPositive := NbPositive +1;
         elsif(Matr(i, j) < 0) then
            NbNegative := NbNegative +1;
         else
            NbZeros := NbZeros +1;
         end if;
      end loop;
      put("Row");
      put(i, 3);
      put(" : Positive :");
      put(NbPositive, 3);
      put(" - Negative :");
      put(NbNegative, 3);
      put(" - Zeros :");
      put(NbZeros, 3);
      New_Line;
      
      NbPositive := 0;
      NbNegative := 0;
      NbZeros := 0;
   end loop;
end task0;
