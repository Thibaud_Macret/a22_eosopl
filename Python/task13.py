from math import factorial
import random

def PascalTriangle(n, rank):
    return (factorial(n)//(factorial(rank)*factorial(n-rank)))

def Bernoulli(n, p):
    q = 1 - p
    for k in range(4):
        result = PascalTriangle(n, k) * pow(p,k) * pow(q,n-k)
        print(round(result, 3))

Bernoulli(3, 0.4)
print()

def experiment(n, p):
    experimentResults = []
    for index in range (n+1):
        experimentResults.append(0)
    nbCoins = 500000

    for coin in range(nbCoins):
        nbTails = 0
        for index in range(n):
            coinValue = random.random()
            if(coinValue < p):
                nbTails += 1
        experimentResults[nbTails] += 1/nbCoins

    for index in range(len(experimentResults)):
        print(round(experimentResults[index], 3))

experiment(3, 0.4)
