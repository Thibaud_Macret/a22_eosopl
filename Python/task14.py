cityList = [["Riga", 307.17, 740000], ["Compi", 125.07, 25000], ["Edo", 479.19, 3900000]]
1
def print_list(list):
    index = 0
    print("Num.\tCity\tArea\tPopulation")
    for city in list:
        print(str(index) + "\t" + str(city[0]) + "\t" + str(city[1]) + "\t" + str(city[2]))
        index += 1

def add_city(list, name, area, pop):
    list = [[name, area, pop]] + list[0:]
    print("Added city " + name)
    return list
    
def add_city_last(list, name, area, pop):
    list.append([name, area, pop])
    print("Added city " + name)
    return list

def del_city_first(list):
    if(len(list) > 0):
        list = list[1:]
        print("Deleted first city")
    else :
        print("Your list is empty !")
    return list

def del_city_last(list):
    try :
        del list[-1]
        print("Deleted last city")
    except IndexError:
        print("Your list is empty !")
    return list

def order_pop_asc(list):
    list.sort(key=lambda x:x[2])
    return(list)


def order_pop_desc(list):
    list.sort(key=lambda x:-x[2])
    return(list)


def exit_program():
    exit()

def input_message():
    return ("\n1. Addition of city to the beginning of list.\n2. Addition of city to the end of list.\n3. Deletion of city from the beginning of list.\n4. Deletion of city from the end of list.\n5. Sorting of cities by amount of inhabitants (ascending).\n6. Sorting of cities by amount of inhabitants (descending).\n7. Exit from the program.\n")

userInput = 0

while(userInput != "7"):
    userInput = input(input_message())
    match userInput:
        case "1":
            name = input("City name")
            area = float(input("City area"))
            pop = int(input("City pop"))
            cityList = add_city(cityList, name, area, pop)
        case "2":
            name = input("City name")
            area = float(input("City area"))
            pop = int(input("City pop"))
            cityList = add_city_last(cityList, name, area, pop)
        case "3":
            cityList = del_city_first(cityList)
        case "4":
            cityList = del_city_last(cityList)
        case "5":
            cityList = order_pop_asc(cityList)
        case "6":
            cityList = order_pop_desc(cityList)
            pass
        case "7":
            exit()
        case other:
            print("Invalid input !")
    print_list(cityList)
