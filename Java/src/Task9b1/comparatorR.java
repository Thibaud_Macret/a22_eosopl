package Task9b1;

import java.util.Comparator;

public class comparatorR implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		if(((employee) o1).getSalary() > ((employee) o2).getSalary()) {return 1;}
		if(((employee) o1).getSalary() < ((employee) o2).getSalary()) {return -1;}
		return 0;
	}

}
