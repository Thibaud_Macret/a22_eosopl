package Task9b1;

import java.util.ArrayList;
import java.util.Collections;

public class firm {
	protected String name;
	protected ArrayList employees = new ArrayList();
	
	firm(String name){
		this.name = name;
	}
	
	public void add(employee employee) {
		employees.add(employee);
	}
	
	public double getMaxSalary() {
		double max = ((employee)(employees.get(0))).getSalary();
		for(Object o : employees) {
			if(((employee)o).getSalary() > max){max = ((employee)o).getSalary();}
		}
		return max;
	}
	
	
	public String toString() {
		int index = 1;
		String concatString = "----- \nFirm : " + this.name + " Amount of employees : " + employees.size();
		while(index <= employees.size()) {
			concatString += "\n" + index + ". " + employees.get(index-1);
			index ++;
		}
		
		concatString += "\nMaximal salary : " + getMaxSalary();
		
		return (concatString);
	}

	public void sortSalariesAsc() {
		Collections.sort(employees);
	}

	public void sortSalariesDesc() {
		employees.sort(new comparatorR());		
	}
}
