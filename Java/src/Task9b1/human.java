package Task9b1;

public class human {
	protected String name;
	protected String surname;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	human(){}
	
	human(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}
	
	human(human human){
		this.name = human.getName();
		this.surname = human.getSurname();
	}
	
	public String toString() {
		return ("Name : " + name + " Surname : " + surname);
	}
}
