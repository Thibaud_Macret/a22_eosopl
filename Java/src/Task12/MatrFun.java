package Task12;

public class MatrFun{
	//synchronized
	public void printRow(int row, int matrix[][]) {
		System.out.print("row " + row + " : ");
		for (int element : matrix[row]) {
			System.out.print(element + " ");
		}
		System.out.println();
	}
}
