package Task12;

public class MatrThread extends Thread {
	int[][] matrix;
	int startRow;
	MatrFun functions;
	
	public MatrThread(int[][] matrix, MatrFun MFun, String name, int startRow, int priority) {
		this.matrix = matrix;
		this.startRow = startRow;
		setName(name);
		setPriority(priority);
		this.functions = MFun;
	}
	
	public void run() {
		int row = startRow;
		
		while(row < matrix.length) {
			try {
				Thread.sleep(0);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
	//		synchronized(functions) {
				this.functions.printRow(row, matrix);
	//		}
			row += 2;
		}
	}
}
