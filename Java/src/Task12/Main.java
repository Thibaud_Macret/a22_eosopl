package Task12;

public class Main {
	public static void main(String[] args) {
		int[][] matrix = {{3, 2, 1, 7}, {9, 11, 5, 4}, {6, 0, 13, 17}, {7, 4, 14, 15}, {0, 11, 5, 4}, {-10, 0, 13, 17}, {7, 21, 14, 135}};

		MatrFun matrFunction = new MatrFun();
		
		(new MatrThread(matrix, matrFunction, "Even", 0, 5)).start();
		(new MatrThread(matrix, matrFunction, "Odd", 1, 5)).start(); 
	}
}
