package Task9a;

public class Main {
	public static void main(String[] args) {
		System.out.println("Hello world");
		
		human human1 = new human("Franc", "Roger");
		human twinH = new human(human1);
		
		employee empl1 = new employee("Richioud", "Amélie", 200.00);
		employee empl2 = new employee("Lerozier", "Marcus", 1000.12);
		employee twinE = new employee(empl2);
		employee empl3 = new employee("Milou", "Tintin", 5000.52);
		employee empl4 = new employee("Cap", "Hadock", 850.35);
		employee empl5 = new employee("Cube", "Steve", 901.00);
		employee empl6 = new employee("Hero", "Brine", 4630.91);
		
		System.out.println(human1);
		System.out.println(twinH);
		System.out.println(empl1);
		System.out.println(empl2);
		System.out.println(twinE);
		
		firm myFirm = new firm("VEF", 70);
		myFirm.add(empl2);
		myFirm.add(empl1);
		myFirm.add(empl3);
		myFirm.add(twinE);
		myFirm.add(empl4);
		myFirm.add(empl5);
		myFirm.add(empl6);
		
		System.out.println(myFirm);
		
		myFirm.sortSalariesAsc();
		
		System.out.println(myFirm);
		
		myFirm.sortSalariesDesc();
		
		System.out.println(myFirm);
	}
}
