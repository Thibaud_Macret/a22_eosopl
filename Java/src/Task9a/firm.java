package Task9a;

import java.util.Arrays;

public class firm {
	protected String name;
	protected employee[] employees;
	protected int nbEmployees;
	
	public int getNbEmployees() {
		return nbEmployees;
	}
	
	public employee[] getEmployees() {
		return employees;
	}
	
	firm(String name, int maxCapacity){
		this.name = name;
		this.employees = new employee[maxCapacity];
		nbEmployees = 0;
	}
	
	public void add(employee employee) {
		try {
				employees[nbEmployees] = employee;
				nbEmployees++;
		} catch (ArrayIndexOutOfBoundsException E) {
			System.out.println(E);
		}
	}
	
	public double getMaxSalary() {
		if(nbEmployees == 0) {return 0;}
		employee max = employees[0];
		for(int index = 0; index < nbEmployees; index++){
			if(employees[index].compareTo(max) == 1) {
				max = employees[index];
			}
		}
		return max.getSalary();
	}
		
	public void sortSalariesAsc() {
		Arrays.sort(employees, 0, nbEmployees);
	}
	
	public void sortSalariesDesc() {
		Arrays.sort(employees, 0, nbEmployees, new comparator());
	}	

	
	public String toString() {
		int index = 1;
		String concatString = "----- \nFirm : " + this.name + " Amount of employees : " + nbEmployees;
		while(index <= employees.length && employees[index-1] != null) {
			concatString += "\n" + index + ". " + employees[index-1];
			index ++;
		}
		
		concatString += "\nMaximal salary : " + getMaxSalary();
		
		return (concatString);
	}
}
