package Task9a;

public class employee extends human implements Comparable{
	private double salary;

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	employee(String name, String surname, double salary) {
		super(name, surname);
		this.salary = salary;
	}
		
	employee(employee employee){
		this.name = employee.getName();
		this.surname = employee.getSurname();
		this.salary = employee.getSalary();
	}
	
	public String toString() {
		return (super.toString() + " Salary : " + salary);
	}

	@Override
	public int compareTo(Object o) {
		if (((employee) o).getSalary() > this.salary) {return -1;}
		if (((employee) o).getSalary() < this.salary) {return 1;}
		return 0;
	}
}
