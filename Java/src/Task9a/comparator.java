package Task9a;

import java.util.Comparator;

public class comparator implements Comparator {

	@Override
	public int compare(Object o1, Object o2) {
		if(o1 != null && o2 != null) {
			if(((employee) o1).getSalary() > ((employee) o2).getSalary()) {return -1;}
			if(((employee) o1).getSalary() < ((employee) o2).getSalary()) {return 1;}
		}
		return 0;
	}

}
