package Task8;

public class employee extends human{
	private double salary;

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}
	
	employee(String name, String surname, double salary) {
		super(name, surname);
		this.salary = salary;
	}
		
	employee(employee employee){
		this.name = employee.getName();
		this.surname = employee.getSurname();
		this.salary = employee.getSalary();
	}
	
	public String toString() {
		return (super.toString() + " Salary : " + salary);
	}
}
