package Task8;

public class firm {
	protected String name;
	protected employee[] employees;
	protected int nbEmployees;
	
	firm(String name, int maxCapacity){
		this.name = name;
		this.employees = new employee[maxCapacity];
		nbEmployees = 0;
	}
	
	public void add(employee employee) {
		try {
				employees[nbEmployees] = employee;
				nbEmployees++;
		} catch (ArrayIndexOutOfBoundsException E) {
			System.out.println(E);
		}
	}
	
	public String toString() {
		int index = 1;
		String concatString = "----- \nFirm : " + this.name + " Amount of employees : " + nbEmployees;
		while(employees[index] != null) {
			concatString += "\n" + index + ". " + employees[index];
			index ++;
		}
		return (concatString);
	}
}
