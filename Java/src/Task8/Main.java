package Task8;

public class Main {
	public static void main(String[] args) {
		System.out.println("Hello world");
		
		human human1 = new human("Franc", "Roger");
		human twinH = new human(human1);
		
		employee empl1 = new employee("Richioud", "Amélie", 2);
		employee empl2 = new employee("Lerozier", "Marcus", 0.1);
		employee twinE = new employee(empl2);
		
		System.out.println(human1);
		System.out.println(twinH);
		System.out.println(empl1);
		System.out.println(empl2);
		System.out.println(twinE);
		
		firm myFirm = new firm("VEF", 10);
		myFirm.add(empl2);
		myFirm.add(empl1);
		myFirm.add(twinE);
		
		System.out.println(myFirm);
	}
}
