package Task9b2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class firm {
	protected String name;
	protected ArrayList<employee> employees = new ArrayList<employee>();
	
	firm(String name){
		this.name = name;
	}
	
	public void add(employee employee) {
		employees.add(employee);
	}
	
	public double getMaxSalary() {
		double max = employees.get(0).getSalary();
		for(employee o : employees) {
			if(o.getSalary() > max){max = o.getSalary();}
		}
		return max;
	}
	
	
	public String toString() {
		int index = 1;
		String concatString = "----- \nFirm : " + this.name + " Amount of employees : " + employees.size();
		while(index <= employees.size()) {
			concatString += "\n" + index + ". " + employees.get(index-1);
			index ++;
		}
		
		concatString += "\nMaximal salary : " + getMaxSalary();
		
		return (concatString);
	}

	public void sortSalariesAsc() {
		Collections.sort(employees);
	}

	public void sortSalariesDesc() {
		employees.sort(new comparator());		
	}
}
