package Task9b2;

import java.util.Comparator;

public class comparatorR implements Comparator<employee> {

	@Override
	public int compare(employee o1, employee o2) {
		if(o1.getSalary() > o2.getSalary()) {return 1;}
		if(o1.getSalary() < o2.getSalary()) {return -1;}
		return 0;
	}

}
