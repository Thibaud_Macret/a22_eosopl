package Task11;

public class Main {
	public static void main(String[] args) {
		int[][] matrix = {{3, 2, 1, 7}, {9, 11, 5, 4}, {6, 0, 13, 17}, {7, 4, 14, 15}, {0, 11, 5, 4}, {-10, 0, 13, 17}, {7, 21, 14, 135}};

		MatrThreadPart2  Odd = new MatrThreadPart2(matrix, 1, "Odd", 2, 0);
		MatrThreadPart2  Even = new MatrThreadPart2(matrix, 0, "Even", 2, 0);
		
		try {
			Odd.getThread().join();
			Even.getThread().join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println("Total Sum :" + (Odd.getSum() + Even.getSum()));
	}
}
