package Task11;

public class MatrThreadA extends Thread {
	int[][] matrix;
	int startRow;
	long delay;
	
	public MatrThreadA(int[][] matrix, int startRow, String name, int priority, long delay) {
		this.matrix = matrix;
		this.startRow = startRow;
		setName(name);
		setPriority(priority);
		this.delay = delay;
	}
	
	public void run() {
		int row = startRow;
				
		while(row < matrix.length) {			
			try {
				Thread.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			int col = 0;
			int sum = 0;
			while(col < matrix[row].length) {
				sum += matrix[row][col];
				col++;
			}
			System.out.println(Thread.currentThread() + " Row : " + row + " Sum : " + sum);
			row += 2;
		}
	}
}
