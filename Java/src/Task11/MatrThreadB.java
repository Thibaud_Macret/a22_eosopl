package Task11;

public class MatrThreadB implements Runnable {
	Thread T;
	int[][] matrix;
	int startRow;
	long delay;
	int sum = 0;
	
	public MatrThreadB(int[][] matrix, int startRow, String name, int priority, long delay) {
		this.matrix = matrix;
		this.startRow = startRow;
		T = new Thread(this);
		T.setName(name);
		T.setPriority(priority);
		this.delay = delay;
		
		T.start();
	}
	
	public void run() {
		int row = startRow;
				
		while(row < matrix.length) {			
			try {
				T.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			int col = 0;
			sum = 0;
			while(col < matrix[row].length) {
				sum += matrix[row][col];
				col++;
			}
			System.out.println(T + " Row : " + row + " Sum : " + sum);
			row += 2;
		}
	}
	
	public Thread getThread() {
		return T;
	}
	
	public int getSum() {
		return sum;
	}
}
