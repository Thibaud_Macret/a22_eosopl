package Task11;

public class MatrThreadPart2 implements Runnable {
	Thread T;
	int[][] matrix;
	int startRow;
	long delay;
	
	public MatrThreadPart2(int[][] matrix, int startRow, String name, int priority, long delay) {
		this.matrix = matrix;
		this.startRow = startRow;
		T = new Thread(this);
		T.setName(name);
		T.setPriority(priority);
		this.delay = delay;
		
		T.start();
	}
	
	public void run() {
		System.out.println(T + " Sum : " + getSum());
	}
	
	public Thread getThread() {
		return T;
	}
	
	public int getSum() {
		int sum = 0;
		int row = startRow;
		
		while(row < matrix.length) {			
			try {
				T.sleep(delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			int col = 0;
			while(col < matrix[row].length) {
				sum += matrix[row][col];
				col++;
			}
			row += 2;
		}
		return sum;
	}
}
